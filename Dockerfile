FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /run/authelia
RUN mkdir -p /app/code
WORKDIR /app/code

ENV VERSION=v3.14.0
RUN npm install authelia@${VERSION}

EXPOSE 3000

COPY start.sh config.template.yml config.custom_template.yml ./yaml-override.js /app/code/

CMD [ "/app/code/start.sh" ]
