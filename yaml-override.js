#!/usr/bin/env node

'use strict';

var fs = require('fs'),
    yaml = require('yamljs');

var target = yaml.load(process.argv[2]);
var source = yaml.load(process.argv[3]);
target = Object.assign(target, source);
fs.writeFileSync(process.argv[2], yaml.dump(target));
