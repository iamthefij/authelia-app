# authelia-cloudron

Authelia app for Cloudron

## Not working

This doesn't work, and I'm pretty sure it never will.

I [Cloudron's nginx config sets](https://git.cloudron.io/cloudron/box/blob/e79b6ade51d683596eed1b9f7894437ec8b23b91/src/appconfig.ejs#L113) `X-Forwarded-*` to that of the requested application. This ends up overwriting the headers set by whatever the original forwarder (eg. Traefik).