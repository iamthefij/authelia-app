#! /bin/bash

set -eu

mkdir -p /run/authelia

# Generate session secret if it doesn't exist
if [[ ! -f /app/data/session_secret ]]; then
    pwgen -1 -s > /app/data/session_secret
fi
SESSION_SECRET=$(cat /app/data/session_secret)

# Generate base config from template
sed -e "s|##DEFAULT_REDIRECT_URL|${WEBADMIN_ORIGIN}|" \
    -e "s|##LDAP_URL|${LDAP_URL}|"\
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/"\
    -e "s/##LDAP_GROUPS_BASE_DN/${LDAP_GROUPS_BASE_DN}/"\
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/"\
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/"\
    -e "s/##SESSION_SECRET/${SESSION_SECRET}/"\
    -e "s/##APP_DOMAIN/${APP_DOMAIN}/"\
    -e "s|##APP_ORIGIN|${APP_ORIGIN}|"\
    -e "s/##REDIS_HOST/${REDIS_HOST}/"\
    -e "s/##REDIS_PORT/${REDIS_PORT}/"\
    -e "s/##REDIS_PASSWORD/${REDIS_PASSWORD}/"\
    -e "s|##MONGODB_URL|${MONGODB_URL}|"\
    -e "s/##MONGODB_DATABASE/${MONGODB_DATABASE}/"\
    -e "s/##MONGODB_USERNAME/${MONGODB_USERNAME}/"\
    -e "s/##MONGODB_PASSWORD/${MONGODB_PASSWORD}/"\
    -e "s/##MAIL_SMTP_USERNAME/${MAIL_SMTP_USERNAME}/"\
    -e "s/##MAIL_SMTP_PASSWORD/${MAIL_SMTP_PASSWORD}/"\
    -e "s/##MAIL_SMTP_SERVER/${MAIL_SMTP_SERVER}/"\
    -e "s/##MAIL_SMTP_PORT/${MAIL_SMTP_PORT}/"\
    -e "s/##MAIL_FROM/${MAIL_FROM}/"\
    /app/code/config.template.yml > /run/authelia/config.yml

# Create override config.yml file if not exists
if [[ ! -f /app/data/config.yml ]]; then
    cp /app/code/config.custom_template.yml /app/data/config.yml
fi

# merge yaml files
/app/code/yaml-override.js /run/authelia/config.yml /app/data/config.yml

chown -R cloudron:cloudron /app/data /run/authelia

exec /usr/local/bin/gosu cloudron:cloudron ./node_modules/.bin/authelia /run/authelia/config.yml
